FROM openjdk:11
ADD target/*.jar order.jar
ENTRYPOINT ["java", "-jar", "order.jar"]