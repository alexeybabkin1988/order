package com.babkin.order.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.type.descriptor.sql.LongVarbinaryTypeDescriptor;

import javax.persistence.*;

@Entity
@Table(name = "orders")
@Getter
@Setter
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private Boolean status;

    public org.babkin.common.models.Order toModel() {
        org.babkin.common.models.Order order = new org.babkin.common.models.Order();
        order.setId(id);
        order.setUserId(userId);
        order.setPrice(price);
        order.setStatus(status);
        return order;
    }

    public static Order fromModel(org.babkin.common.models.Order order) {
        Order order1 = new Order();
        order1.setId(order.getId());
        order1.setUserId(order.getUserId());
        order1.setPrice(order.getPrice());
        order1.setStatus(order.getStatus());
        return order1;
    }
}
