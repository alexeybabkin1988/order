package com.babkin.order.controller;

import com.babkin.order.service.OrderService;
import org.babkin.common.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping(value = "/producer")
    public ResponseEntity<Order> send(@RequestBody Order order) {
        orderService.saveOrder(order);
        orderService.send(order);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @GetMapping(value = "/home")
    public String home() {
        return "Private page ...";
    }
}
