package com.babkin.order.service;

import com.babkin.order.repository.OrderRepository;
import org.babkin.common.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;


@Service
public class OrderService {

    @Value("${kafka.topic}")
    private String topic;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private KafkaTemplate<String, Order> kafkaTemplate;

    public com.babkin.order.domain.Order saveOrder(Order order) {
        com.babkin.order.domain.Order order1 = com.babkin.order.domain.Order.fromModel(order);
        order1.setId(null);
        return orderRepository.save(order1);
    }

    public void send(Order order) {
        kafkaTemplate.send(topic, order);
    }

    @KafkaListener(topics = "${kafka.payment-topic}")
    public void pool(Order order) {
        com.babkin.order.domain.Order order1 = orderRepository.findById(order.getId())
                .orElseThrow(EntityNotFoundException::new);
        order1.setStatus(order.getStatus());
        orderRepository.save(order1);
    }
}
